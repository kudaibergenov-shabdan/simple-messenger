const express = require('express');
const {nanoid} = require("nanoid");
const route = express.Router();
const fileDb = require('../fileDb');

route.get('/', (req, res) => {
  if (!req.query.datetime) {
    const messages = fileDb.getItems();
    if (messages.length > 0) {
      return res.send(messages);
    } else {
      return res.status(400).send({"error" : "No data found"});
    }
  } else {
    if (!isNaN(new Date(req.query.datetime))) {
      const message = fileDb.getItem(req.query.datetime);
      if (message) {
        return res.send(message)
      } else {
        return res.status(400).send({"error": "No messages with such a datetime"});
      }
    } else {
      return res.status(400).send({"error" : "Invalid datetime"});
    }
  }
})

route.post('/', (req, res) => {
  if (!req.body.author || !req.body.message) {
    return res.status(400).send({"error": "Author and message must be present in the request"});
  }
  const newMessage = fileDb.addItem({
    id: nanoid(),
    message: req.body.message,
    author: req.body.author,
    datetime: (new Date()).toISOString()
  })
  res.send(newMessage);
});

module.exports = route;