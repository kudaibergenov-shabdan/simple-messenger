const express = require('express');
const cors = require('cors');
const messages = require('./app/messages');
const fileDb = require('./fileDb');

const port = 8000;
const app = express();
app.use(express.json());
app.use(cors());
app.use('/messages', messages);

fileDb.init();

app.listen(port, () => {
  console.log(`Server started on ${port} successfully`);
});

