const fs = require('fs');
const fileName = './messagesRepository/db.json';
let data = [];

module.exports = {
  init() {
    try {
      const fileContents = fs.readFileSync(fileName);
      data = JSON.parse(fileContents);
    } catch (e) {
      data = []
    }
  },

  getItems () {
    return data.slice(-30);
  },

  getItem(datetime) {
    return data.filter(i => i.datetime > datetime);
  },

  addItem(item) {
    data.push(item);
    this.save();
    const {id, datetime} = item;
    return {id, datetime};
  },

  save() {
    fs.writeFileSync(fileName, JSON.stringify(data));
  }
}
