import React from 'react';
import './MessageSender.css';
import {useDispatch, useSelector} from "react-redux";
import {changeText, sendMessage} from "../../store/actions/messagesAction";

const MessageSender = () => {
    const dispatch = useDispatch();
    const messageText = useSelector(state => state.messages.messageText);
    const authorName = useSelector(state => state.messages.authorName);

    const _onInputChange = e => {
        const {name, value} = e.target;
        dispatch(changeText(name, value));
    };

    const _sendMessage = e => {
        e.preventDefault();
        if (authorName && messageText) {
            dispatch(sendMessage({
                author: authorName,
                message: messageText
            }));
        }
    };

    return (
        <form className="MessageSender">
            <textarea
                className="MessageSender__message-textarea"
                name="messageText"
                value={messageText}
                onChange={_onInputChange}
                placeholder="Type your text here"
            />
            <div className="MessageSender__message-author">
                <input
                    type="text"
                    name="authorName"
                    value={authorName}
                    onChange={_onInputChange}
                    placeholder="Author name"
                />
                <button
                    className="MessageSender__btn-send"
                    type="submit"
                    onClick={_sendMessage}
                >
                    Send
                </button>
            </div>
        </form>
    );
};

export default MessageSender;