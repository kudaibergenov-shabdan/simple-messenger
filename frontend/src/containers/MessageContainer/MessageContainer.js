import React, {useEffect} from 'react';
import './MessageContainer.css';
import {useDispatch, useSelector} from "react-redux";
import {fetchMessages} from "../../store/actions/messagesAction";
import Message from "../../components/Message/Message";

const MessageContainer = () => {
    const dispatch = useDispatch();
    const messages = useSelector(state => state.messages.messagesArray);

    useEffect(() => {
        dispatch(fetchMessages());
    }, [dispatch]);

    return (
        <div className="MessageContainer">
            {messages.map(message => (
                <Message
                    key={message.id}
                    messageObj={message}
                />
            )).reverse()}
        </div>
    );
};

export default MessageContainer;