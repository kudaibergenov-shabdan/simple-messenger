import MessageContainer from "./containers/MessageContainer/MessageContainer";
import MessageSender from "./containers/MessageSender/MessageSender";

const App = () => (
    <>
        <MessageSender />
        <MessageContainer/>
    </>
);

export default App;
