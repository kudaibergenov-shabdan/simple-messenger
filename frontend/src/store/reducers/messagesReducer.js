import {CHANGE_MESSAGE_TEXT, FETCH_LAST_MESSAGES_SUCCESS, FETCH_MESSAGES_SUCCESS} from "../actions/messagesAction";

const initialState = {
    authorName: "",
    messageText: "",
    messagesArray: []
};

const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MESSAGES_SUCCESS:
            return {...state, messagesArray: action.payload}
        case FETCH_LAST_MESSAGES_SUCCESS:
            return {...state, messagesArray: [...state.messagesArray].concat(action.payload)};
        case CHANGE_MESSAGE_TEXT:
            return {...state, [action.payload.name]: action.payload.text}
        default:
            return state;
    }
};

export default messagesReducer;