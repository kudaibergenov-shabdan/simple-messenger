import React from 'react';
import './Message.css';

const Message = ({messageObj}) => {
    return (
        <div className="Message">
            <p className="Message__paragraph">
                <span className="Author">{messageObj.author} said ({messageObj.datetime}):</span>
            </p>
            <p className="Message__paragraph Message__paragraph_spacing">
                <span className="Author">{messageObj.message}</span>
            </p>
        </div>
    );
};

export default Message;